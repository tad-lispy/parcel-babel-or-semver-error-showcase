# Parcel Babel or SemVer error showcase

This repository demonstrates a bug somewhere between Parcel, Babel and SemVer.

``` text
$ ./node_modules/.bin/parcel src/index.html
Server running at http://localhost:1234
🚨  /snake-game/src/index.html: Invalid Version: undefined
    at new SemVer (/snake-game/node_modules/@babel/preset-env/node_modules/semver/semver.js:314:11)
    at compare (/snake-game/node_modules/@babel/preset-env/node_modules/semver/semver.js:647:10)
    at lt (/snake-game/node_modules/@babel/preset-env/node_modules/semver/semver.js:688:10)
    at /snake-game/node_modules/@babel/preset-env/lib/index.js:276:22
    at Object.default (/snake-game/node_modules/@babel/helper-plugin-utils/lib/index.js:22:12)
    at getEnvPlugins (/snake-game/node_modules/parcel/src/transforms/babel/env.js:62:34)
    at getEnvConfig (/snake-game/node_modules/parcel/src/transforms/babel/env.js:12:25)
    at async getBabelConfig (/snake-game/node_modules/parcel/src/transforms/babel/config.js:32:19)
    at async babelTransform (/snake-game/node_modules/parcel/src/transforms/babel/transform.js:6:16)
    at async JSAsset.pretransform (/snake-game/node_modules/parcel/src/assets/JSAsset.js:83:5)
```

The problem occured for the first time today. A project with identical contents except for `package-lock.json` generated yesterday works without issues. Same if I copy the `package.json` and `package-lock.json` from this other project here.

Likely the problem is introduced by bumping `@babel/preset-env` from `7.13.8` to `7.13.9`. Here is a JSON diff of `package-lock.json`: 

``` diff
@ ["dependencies","@babel/generator","integrity"]
- "sha512-zBZfgvBB/ywjx0Rgc2+BwoH/3H+lDtlgD4hBOpEv5LxRnYsm/753iRuLepqnYlynpjC3AdQxtxsoeHJoEEwOAw=="
+ "sha512-mHOOmY0Axl/JCTkxTU6Lf5sWOg/v8nUa+Xkt4zMTftX0wqmb6Sh7J8gvcehBw7q0AhrhAR+FDacKjCZ2X8K+Sw=="
@ ["dependencies","@babel/generator","resolved"]
- "https://registry.npmjs.org/@babel/generator/-/generator-7.13.0.tgz"
+ "https://registry.npmjs.org/@babel/generator/-/generator-7.13.9.tgz"
@ ["dependencies","@babel/generator","version"]
- "7.13.0"
+ "7.13.9"
@ ["dependencies","@babel/parser","integrity"]
- "sha512-uvoOulWHhI+0+1f9L4BoozY7U5cIkZ9PgJqvb041d6vypgUmtVPG4vmGm4pSggjl8BELzvHyUeJSUyEMY6b+qA=="
+ "sha512-nEUfRiARCcaVo3ny3ZQjURjHQZUo/JkEw7rLlSZy/psWGnvwXFtPcr6jb7Yb41DVW5LTe6KRq9LGleRNsg1Frw=="
@ ["dependencies","@babel/parser","resolved"]
- "https://registry.npmjs.org/@babel/parser/-/parser-7.13.4.tgz"
+ "https://registry.npmjs.org/@babel/parser/-/parser-7.13.9.tgz"
@ ["dependencies","@babel/parser","version"]
- "7.13.4"
+ "7.13.9"
@ ["dependencies","@babel/preset-env","integrity"]
- "sha512-Sso1xOpV4S3ofnxW2DsWTE5ziRk62jEAKLGuQ+EJHC+YHTbFG38QUTixO3JVa1cYET9gkJhO1pMu+/+2dDhKvw=="
+ "sha512-mcsHUlh2rIhViqMG823JpscLMesRt3QbMsv1+jhopXEb3W2wXvQ9QoiOlZI9ZbR3XqPtaFpZwEZKYqGJnGMZTQ=="
@ ["dependencies","@babel/preset-env","resolved"]
- "https://registry.npmjs.org/@babel/preset-env/-/preset-env-7.13.8.tgz"
+ "https://registry.npmjs.org/@babel/preset-env/-/preset-env-7.13.9.tgz"
@ ["dependencies","@babel/preset-env","version"]
- "7.13.8"
+ "7.13.9"
@ ["dependencies","@babel/runtime","integrity"]
- "sha512-CwQljpw6qSayc0fRG1soxHAKs1CnQMOChm4mlQP6My0kf9upVGizj/KhlTTgyUnETmHpcUXjaluNAkteRFuafg=="
+ "sha512-aY2kU+xgJ3dJ1eU6FMB9EH8dIe8dmusF1xEku52joLvw6eAFN0AI+WxCLDnpev2LEejWBAy2sBvBOBAjI3zmvA=="
@ ["dependencies","@babel/runtime","resolved"]
- "https://registry.npmjs.org/@babel/runtime/-/runtime-7.13.8.tgz"
+ "https://registry.npmjs.org/@babel/runtime/-/runtime-7.13.9.tgz"
@ ["dependencies","@babel/runtime","version"]
- "7.13.8"
+ "7.13.9"
@ ["dependencies","caniuse-lite","integrity"]
- "sha512-63OrUnwJj5T1rUmoyqYTdRWBqFFxZFlyZnRRjDR8NSUQFB6A+j/uBORU/SyJ5WzDLg4SPiZH40hQCBNdZ/jmAw=="
+ "sha512-iDUOH+oFeBYk5XawYsPtsx/8fFpndAPUQJC7gBTfxHM8xw5nOZv7ceAD4frS1MKCLUac7QL5wdAJiFQlDRjXlA=="
@ ["dependencies","caniuse-lite","resolved"]
- "https://registry.npmjs.org/caniuse-lite/-/caniuse-lite-1.0.30001192.tgz"
+ "https://registry.npmjs.org/caniuse-lite/-/caniuse-lite-1.0.30001194.tgz"
@ ["dependencies","caniuse-lite","version"]
- "1.0.30001192"
+ "1.0.30001194"
@ ["dependencies","electron-to-chromium","integrity"]
- "sha512-GEQw+6dNWjueXGkGfjgm7dAMtXfEqrfDG3uWcZdeaD4cZ3dKYdPRQVruVXQRXtPLtOr5GNVVlNLRMChOZ611pQ=="
+ "sha512-Tcmk+oKQgpjcM+KYanlkd76ZtpzalkpUULnlJDP6vjHtR7UU564IM9Qv5DxqHZNBQjzXm6mkn7Y8bw2OoE3FmQ=="
@ ["dependencies","electron-to-chromium","resolved"]
- "https://registry.npmjs.org/electron-to-chromium/-/electron-to-chromium-1.3.675.tgz"
+ "https://registry.npmjs.org/electron-to-chromium/-/electron-to-chromium-1.3.677.tgz"
@ ["dependencies","electron-to-chromium","version"]
- "1.3.675"
+ "1.3.677"
@ ["dependencies","es-abstract","integrity"]
- "sha512-Ih4ZMFHEtZupnUh6497zEL4y2+w8+1ljnCyaTa+adcoafI1GOvMwFlDjBLfWR7y9VLfrjRJe9ocuHY1PSR9jjw=="
+ "sha512-VMzHx/Bczjg59E6jZOQjHeN3DEoptdhejpARgflAViidlqSpjdq9zA6lKwlhRRs/lOw1gHJv2xkkSFRgvEwbQg=="
@ ["dependencies","es-abstract","requires","get-intrinsic"]
- "^1.0.2"
+ "^1.1.1"
@ ["dependencies","es-abstract","requires","has-symbols"]
- "^1.0.1"
+ "^1.0.2"
@ ["dependencies","es-abstract","requires","is-callable"]
- "^1.2.2"
+ "^1.2.3"
@ ["dependencies","es-abstract","requires","is-regex"]
- "^1.1.1"
+ "^1.1.2"
@ ["dependencies","es-abstract","requires","string.prototype.trimend"]
- "^1.0.3"
+ "^1.0.4"
@ ["dependencies","es-abstract","requires","string.prototype.trimstart"]
- "^1.0.3"
+ "^1.0.4"
@ ["dependencies","es-abstract","requires","is-string"]
+ "^1.0.5"
@ ["dependencies","es-abstract","requires","unbox-primitive"]
+ "^1.0.0"
@ ["dependencies","es-abstract","resolved"]
- "https://registry.npmjs.org/es-abstract/-/es-abstract-1.18.0-next.2.tgz"
+ "https://registry.npmjs.org/es-abstract/-/es-abstract-1.18.0-next.3.tgz"
@ ["dependencies","es-abstract","version"]
- "1.18.0-next.2"
+ "1.18.0-next.3"
@ ["dependencies","has-bigints"]
+ {"dev":true,"integrity":"sha512-LSBS2LjbNBTf6287JEbEzvJgftkF5qFkmCo9hDRpAzKhUOlJ+hx8dd4USs00SgsUNwc4617J9ki5YtEClM2ffA==","resolved":"https://registry.npmjs.org/has-bigints/-/has-bigints-1.0.1.tgz","version":"1.0.1"}
@ ["dependencies","is-bigint"]
+ {"dev":true,"integrity":"sha512-J0ELF4yHFxHy0cmSxZuheDOz2luOdVvqjwmEcj8H/L1JHeuEDSDbeRP+Dk9kFVk5RTFzbucJ2Kb9F7ixY2QaCg==","resolved":"https://registry.npmjs.org/is-bigint/-/is-bigint-1.0.1.tgz","version":"1.0.1"}
@ ["dependencies","is-boolean-object"]
+ {"dev":true,"integrity":"sha512-a7Uprx8UtD+HWdyYwnD1+ExtTgqQtD2k/1yJgtXP6wnMm8byhkoTZRl+95LLThpzNZJ5aEvi46cdH+ayMFRwmA==","requires":{"call-bind":"^1.0.0"},"resolved":"https://registry.npmjs.org/is-boolean-object/-/is-boolean-object-1.1.0.tgz","version":"1.1.0"}
@ ["dependencies","is-number-object"]
+ {"dev":true,"integrity":"sha512-zohwelOAur+5uXtk8O3GPQ1eAcu4ZX3UwxQhUlfFFMNpUd83gXgjbhJh6HmB6LUNV/ieOLQuDwJO3dWJosUeMw==","resolved":"https://registry.npmjs.org/is-number-object/-/is-number-object-1.0.4.tgz","version":"1.0.4"}
@ ["dependencies","is-string"]
+ {"dev":true,"integrity":"sha512-buY6VNRjhQMiF1qWDouloZlQbRhDPCebwxSjxMjxgemYT46YMd2NR0/H+fBhEfWX4A/w9TBJ+ol+okqJKFE6vQ==","resolved":"https://registry.npmjs.org/is-string/-/is-string-1.0.5.tgz","version":"1.0.5"}
@ ["dependencies","unbox-primitive"]
+ {"dev":true,"integrity":"sha512-P/51NX+JXyxK/aigg1/ZgyccdAxm5K1+n8+tvqSntjOivPt19gvm1VC49RWYetsiub8WViUchdxl/KWHHB0kzA==","requires":{"function-bind":"^1.1.1","has-bigints":"^1.0.0","has-symbols":"^1.0.0","which-boxed-primitive":"^1.0.1"},"resolved":"https://registry.npmjs.org/unbox-primitive/-/unbox-primitive-1.0.0.tgz","version":"1.0.0"}
@ ["dependencies","which-boxed-primitive"]
+ {"dev":true,"integrity":"sha512-bwZdv0AKLpplFY2KZRX6TvyuN7ojjr7lwkg6ml0roIy9YeuSr7JS372qlNW18UQYzgYK9ziGcerWqZOmEn9VNg==","requires":{"is-bigint":"^1.0.1","is-boolean-object":"^1.1.0","is-number-object":"^1.0.4","is-string":"^1.0.5","is-symbol":"^1.0.3"},"resolved":"https://registry.npmjs.org/which-boxed-primitive/-/which-boxed-primitive-1.0.2.tgz","version":"1.0.2"}
@ ["packages","node_modules/@babel/generator","integrity"]
- "sha512-zBZfgvBB/ywjx0Rgc2+BwoH/3H+lDtlgD4hBOpEv5LxRnYsm/753iRuLepqnYlynpjC3AdQxtxsoeHJoEEwOAw=="
+ "sha512-mHOOmY0Axl/JCTkxTU6Lf5sWOg/v8nUa+Xkt4zMTftX0wqmb6Sh7J8gvcehBw7q0AhrhAR+FDacKjCZ2X8K+Sw=="
@ ["packages","node_modules/@babel/generator","resolved"]
- "https://registry.npmjs.org/@babel/generator/-/generator-7.13.0.tgz"
+ "https://registry.npmjs.org/@babel/generator/-/generator-7.13.9.tgz"
@ ["packages","node_modules/@babel/generator","version"]
- "7.13.0"
+ "7.13.9"
@ ["packages","node_modules/@babel/parser","integrity"]
- "sha512-uvoOulWHhI+0+1f9L4BoozY7U5cIkZ9PgJqvb041d6vypgUmtVPG4vmGm4pSggjl8BELzvHyUeJSUyEMY6b+qA=="
+ "sha512-nEUfRiARCcaVo3ny3ZQjURjHQZUo/JkEw7rLlSZy/psWGnvwXFtPcr6jb7Yb41DVW5LTe6KRq9LGleRNsg1Frw=="
@ ["packages","node_modules/@babel/parser","resolved"]
- "https://registry.npmjs.org/@babel/parser/-/parser-7.13.4.tgz"
+ "https://registry.npmjs.org/@babel/parser/-/parser-7.13.9.tgz"
@ ["packages","node_modules/@babel/parser","version"]
- "7.13.4"
+ "7.13.9"
@ ["packages","node_modules/@babel/preset-env","integrity"]
- "sha512-Sso1xOpV4S3ofnxW2DsWTE5ziRk62jEAKLGuQ+EJHC+YHTbFG38QUTixO3JVa1cYET9gkJhO1pMu+/+2dDhKvw=="
+ "sha512-mcsHUlh2rIhViqMG823JpscLMesRt3QbMsv1+jhopXEb3W2wXvQ9QoiOlZI9ZbR3XqPtaFpZwEZKYqGJnGMZTQ=="
@ ["packages","node_modules/@babel/preset-env","resolved"]
- "https://registry.npmjs.org/@babel/preset-env/-/preset-env-7.13.8.tgz"
+ "https://registry.npmjs.org/@babel/preset-env/-/preset-env-7.13.9.tgz"
@ ["packages","node_modules/@babel/preset-env","version"]
- "7.13.8"
+ "7.13.9"
@ ["packages","node_modules/@babel/runtime","integrity"]
- "sha512-CwQljpw6qSayc0fRG1soxHAKs1CnQMOChm4mlQP6My0kf9upVGizj/KhlTTgyUnETmHpcUXjaluNAkteRFuafg=="
+ "sha512-aY2kU+xgJ3dJ1eU6FMB9EH8dIe8dmusF1xEku52joLvw6eAFN0AI+WxCLDnpev2LEejWBAy2sBvBOBAjI3zmvA=="
@ ["packages","node_modules/@babel/runtime","resolved"]
- "https://registry.npmjs.org/@babel/runtime/-/runtime-7.13.8.tgz"
+ "https://registry.npmjs.org/@babel/runtime/-/runtime-7.13.9.tgz"
@ ["packages","node_modules/@babel/runtime","version"]
- "7.13.8"
+ "7.13.9"
@ ["packages","node_modules/caniuse-lite","integrity"]
- "sha512-63OrUnwJj5T1rUmoyqYTdRWBqFFxZFlyZnRRjDR8NSUQFB6A+j/uBORU/SyJ5WzDLg4SPiZH40hQCBNdZ/jmAw=="
+ "sha512-iDUOH+oFeBYk5XawYsPtsx/8fFpndAPUQJC7gBTfxHM8xw5nOZv7ceAD4frS1MKCLUac7QL5wdAJiFQlDRjXlA=="
@ ["packages","node_modules/caniuse-lite","resolved"]
- "https://registry.npmjs.org/caniuse-lite/-/caniuse-lite-1.0.30001192.tgz"
+ "https://registry.npmjs.org/caniuse-lite/-/caniuse-lite-1.0.30001194.tgz"
@ ["packages","node_modules/caniuse-lite","version"]
- "1.0.30001192"
+ "1.0.30001194"
@ ["packages","node_modules/electron-to-chromium","integrity"]
- "sha512-GEQw+6dNWjueXGkGfjgm7dAMtXfEqrfDG3uWcZdeaD4cZ3dKYdPRQVruVXQRXtPLtOr5GNVVlNLRMChOZ611pQ=="
+ "sha512-Tcmk+oKQgpjcM+KYanlkd76ZtpzalkpUULnlJDP6vjHtR7UU564IM9Qv5DxqHZNBQjzXm6mkn7Y8bw2OoE3FmQ=="
@ ["packages","node_modules/electron-to-chromium","resolved"]
- "https://registry.npmjs.org/electron-to-chromium/-/electron-to-chromium-1.3.675.tgz"
+ "https://registry.npmjs.org/electron-to-chromium/-/electron-to-chromium-1.3.677.tgz"
@ ["packages","node_modules/electron-to-chromium","version"]
- "1.3.675"
+ "1.3.677"
@ ["packages","node_modules/es-abstract","dependencies","get-intrinsic"]
- "^1.0.2"
+ "^1.1.1"
@ ["packages","node_modules/es-abstract","dependencies","has-symbols"]
- "^1.0.1"
+ "^1.0.2"
@ ["packages","node_modules/es-abstract","dependencies","is-callable"]
- "^1.2.2"
+ "^1.2.3"
@ ["packages","node_modules/es-abstract","dependencies","is-regex"]
- "^1.1.1"
+ "^1.1.2"
@ ["packages","node_modules/es-abstract","dependencies","string.prototype.trimend"]
- "^1.0.3"
+ "^1.0.4"
@ ["packages","node_modules/es-abstract","dependencies","string.prototype.trimstart"]
- "^1.0.3"
+ "^1.0.4"
@ ["packages","node_modules/es-abstract","dependencies","is-string"]
+ "^1.0.5"
@ ["packages","node_modules/es-abstract","dependencies","unbox-primitive"]
+ "^1.0.0"
@ ["packages","node_modules/es-abstract","integrity"]
- "sha512-Ih4ZMFHEtZupnUh6497zEL4y2+w8+1ljnCyaTa+adcoafI1GOvMwFlDjBLfWR7y9VLfrjRJe9ocuHY1PSR9jjw=="
+ "sha512-VMzHx/Bczjg59E6jZOQjHeN3DEoptdhejpARgflAViidlqSpjdq9zA6lKwlhRRs/lOw1gHJv2xkkSFRgvEwbQg=="
@ ["packages","node_modules/es-abstract","resolved"]
- "https://registry.npmjs.org/es-abstract/-/es-abstract-1.18.0-next.2.tgz"
+ "https://registry.npmjs.org/es-abstract/-/es-abstract-1.18.0-next.3.tgz"
@ ["packages","node_modules/es-abstract","version"]
- "1.18.0-next.2"
+ "1.18.0-next.3"
@ ["packages","node_modules/has-bigints"]
+ {"dev":true,"funding":{"url":"https://github.com/sponsors/ljharb"},"integrity":"sha512-LSBS2LjbNBTf6287JEbEzvJgftkF5qFkmCo9hDRpAzKhUOlJ+hx8dd4USs00SgsUNwc4617J9ki5YtEClM2ffA==","resolved":"https://registry.npmjs.org/has-bigints/-/has-bigints-1.0.1.tgz","version":"1.0.1"}
@ ["packages","node_modules/is-bigint"]
+ {"dev":true,"funding":{"url":"https://github.com/sponsors/ljharb"},"integrity":"sha512-J0ELF4yHFxHy0cmSxZuheDOz2luOdVvqjwmEcj8H/L1JHeuEDSDbeRP+Dk9kFVk5RTFzbucJ2Kb9F7ixY2QaCg==","resolved":"https://registry.npmjs.org/is-bigint/-/is-bigint-1.0.1.tgz","version":"1.0.1"}
@ ["packages","node_modules/is-boolean-object"]
+ {"dependencies":{"call-bind":"^1.0.0"},"dev":true,"engines":{"node":"\u003e= 0.4"},"funding":{"url":"https://github.com/sponsors/ljharb"},"integrity":"sha512-a7Uprx8UtD+HWdyYwnD1+ExtTgqQtD2k/1yJgtXP6wnMm8byhkoTZRl+95LLThpzNZJ5aEvi46cdH+ayMFRwmA==","resolved":"https://registry.npmjs.org/is-boolean-object/-/is-boolean-object-1.1.0.tgz","version":"1.1.0"}
@ ["packages","node_modules/is-number-object"]
+ {"dev":true,"engines":{"node":"\u003e= 0.4"},"funding":{"url":"https://github.com/sponsors/ljharb"},"integrity":"sha512-zohwelOAur+5uXtk8O3GPQ1eAcu4ZX3UwxQhUlfFFMNpUd83gXgjbhJh6HmB6LUNV/ieOLQuDwJO3dWJosUeMw==","resolved":"https://registry.npmjs.org/is-number-object/-/is-number-object-1.0.4.tgz","version":"1.0.4"}
@ ["packages","node_modules/is-string"]
+ {"dev":true,"engines":{"node":"\u003e= 0.4"},"funding":{"url":"https://github.com/sponsors/ljharb"},"integrity":"sha512-buY6VNRjhQMiF1qWDouloZlQbRhDPCebwxSjxMjxgemYT46YMd2NR0/H+fBhEfWX4A/w9TBJ+ol+okqJKFE6vQ==","resolved":"https://registry.npmjs.org/is-string/-/is-string-1.0.5.tgz","version":"1.0.5"}
@ ["packages","node_modules/unbox-primitive"]
+ {"dependencies":{"function-bind":"^1.1.1","has-bigints":"^1.0.0","has-symbols":"^1.0.0","which-boxed-primitive":"^1.0.1"},"dev":true,"integrity":"sha512-P/51NX+JXyxK/aigg1/ZgyccdAxm5K1+n8+tvqSntjOivPt19gvm1VC49RWYetsiub8WViUchdxl/KWHHB0kzA==","resolved":"https://registry.npmjs.org/unbox-primitive/-/unbox-primitive-1.0.0.tgz","version":"1.0.0"}
@ ["packages","node_modules/which-boxed-primitive"]
+ {"dependencies":{"is-bigint":"^1.0.1","is-boolean-object":"^1.1.0","is-number-object":"^1.0.4","is-string":"^1.0.5","is-symbol":"^1.0.3"},"dev":true,"funding":{"url":"https://github.com/sponsors/ljharb"},"integrity":"sha512-bwZdv0AKLpplFY2KZRX6TvyuN7ojjr7lwkg6ml0roIy9YeuSr7JS372qlNW18UQYzgYK9ziGcerWqZOmEn9VNg==","resolved":"https://registry.npmjs.org/which-boxed-primitive/-/which-boxed-primitive-1.0.2.tgz","version":"1.0.2"}

```



## Author

[Tad Lispy](https://tad-lispy.com)
